window.IllustratorScriptExport = {
    
    export : function(quote, font, size, leading, letterspacing, firstcolor, secondcolor){
    
    
        /*
        function pixeltopoint(pixel){
            return pixel * 1;
            return pixel * .75;
        }

        function leading(fontsize){
            return fontsize * 1.6;
        }

        var quote = "IT'S ADVERTISING MARRYING MINIMALISM AND HAVING\nROSEMARYS BABY";
        var poster_w = 360;
        var poster_h = 509;
        var poster_padding = 30;
        var text_size = pixeltopoint(26);
        var font_family = "Josefin Sans";
        var textwidth = poster_w - (poster_padding*2);
        var textheight = poster_h - (poster_padding*2);

        var myDocument = app.documents.add(DocumentColorSpace.RGB, poster_w, poster_h);

        var x = myDocument.width * .5 - (poster_w *.5);
        var y = myDocument.height *.5 + (poster_h *.5);

        var artLayer = myDocument.layers.add();
        var rect = artLayer.pathItems.rectangle( y, x, poster_w, poster_h );
        rect.stroked = false;

        var backgColor = new RGBColor();
        backgColor.red = 255;
        backgColor.blue = 255;
        backgColor.red = 0;

        rect.fillColor = backgColor;

        var textLayer = myDocument.layers.add();
        var textRect = textLayer.pathItems.rectangle(0, 0, textwidth, textheight);
        var textRef = textLayer.textFrames.areaText(textRect);

        textRef.contents = quote;
        textRef.top = myDocument.height * .5 + textRef.height * .5;
        textRef.left = (myDocument.width * .5) - (textwidth*.5);

        // set character attributes
        // Create a new character style
        for( var i =0; i < textRef.paragraphs.length; i++){
            var paraAttr = textRef.paragraphs[i].paragraphAttributes;
            paraAttr.justification = Justification.CENTER;
            paraAttr.hyphenation = false;
        }

        //

        // Create a color for both ends of the gradient

        var startColor = new RGBColor();
        var endColor = new RGBColor();
        startColor.red = 88;
        startColor.green = 109;
        startColor.blue = 182;

        endColor.red = 151;
        endColor.green = 117;
        endColor.blue = 159;

        // A new gradient always has 2 stops

        var newGradient = app.activeDocument.gradients.add();
        newGradient.name = "NewGradient";
        newGradient.type = GradientType.LINEAR;
        //newGradient.gradientStops[0].rampPoint = 80;
        newGradient.gradientStops[0].color = startColor;

        //newGradient.gradientStops[1].rampPoint = 80;
        newGradient.gradientStops[1].color = endColor;

        // construct an Illustrator.GradientColor object referring to the 
        var colorOfGradient = new GradientColor();
        //colorOfGradient.angle = 60.0;
        colorOfGradient.gradient = newGradient;

        // apply to rect
        rect.filled = true;
        rect.fillColor = colorOfGradient;
        rect.rotate ( 90, false, false, true, false, Transformation.CENTER );

        // createOutline

        var charStyle = myDocument.characterStyles.add("character-style");

        var charAttr = charStyle.characterAttributes;
        charAttr.size = text_size;
        charAttr.autoLeading = false;
        charAttr.leading = leading(text_size);
        charAttr.capitalization = FontCapsOption.ALLCAPS;

        for(var i =0; i < app.textFonts.length; i++){
            if(app.textFonts[i].family == font_family){
                charAttr.textFont = app.textFonts[i];
                break;
            }
        }


        var textColor = new RGBColor();
        textColor.red = 20;
        textColor.green = 20;
        textColor.blue = 20;

        charAttr.fillColor = textColor;

        charStyle.applyTo(textRef.textRange);

        var mycopy = textRef.duplicate();

        var outlines = mycopy.createOutline();

        textRef.top = (myDocument.height * .5) + (outlines.height * .5)+4;
        textRef.textPath.height = outlines.height+10;

        outlines.remove();
        */
    
    
    }
    

}