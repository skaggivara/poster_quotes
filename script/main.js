var MODE = "free";
var FONTS = [];

var DEFAULT_SIZE = 26;
var BLACK = "#171717";
var WHITE = "#f9f9f9";
var POSTER_WIDTH = 360;
var POSTER_HEIGHT = 514;
var PADDING = 30;

var IGNORE = [
    "Apple Color Emoji", 
    "Noteworthy", 
    "Hiragino Sans GB", 
    "Wingdings 1", 
    "Wingdings 2", 
    "Wingdings 3", 
    "Marker Felt", 
    "Tekton Pro", 
    "Sinhala Sangam MN",
    "Microsoft Sans Serif"
];
    
var FLASH_ID = "font-detect-swf";
var FLASH_PATH = "flash/FontList.swf";
    
var QUOTES = [
    "IT'S OKAY TO WANT TO STOP BEING AN INDIVIDUAL",
    "THE INTERNET ALLOWS YOU TO DREAM WHILE YOU'RE STILL AWAKE",
    "LIVES ARE NO LONGER FEELING LIKE STORIES",
    "A FULLY LINKED WORLD<br>NO LONGER NEEDS<br>A MIDDLE CLASS",
    "HOARD ANYTHING YOU CAN'T DOWNLOAD",
    "OUR ONLY HOPE IS TO INVENT SOMETHING SMARTER THAN OURSELVES",
    "THAT SICKENING FEELING WHEN YOU REALIZE YOU'VE LOST YOUR CELL PHONE WILL SOON BE PERMANENT",
    "YOU KNOW THE FUTURE IS REALLY HAPPENING WHEN YOU START FEELING SCARED",
    "REFUSING TO USE A CELL PHONE IS MERELY PASSIVE AGGRESSION",
    "IN THE FUTURE WE'LL ALL BE SHOPPING FROM JAIL",
    "THE THOUGHT OF BEING LESS CONNECTED THAN YOU ARE RIGHT NOW IS IMPOSSIBLE",
    "KNOWING EVERYTHING TURNS OUT TO BE SLIGHTLY BORING",
    "BUDDIST<br>BUBBLEGUM",
    "NOBODY KNOWS ANYTHING",
    "TURN ON<br>TUNE IN<br>DROP OUT",
    "TURN ON<br>BOOT UP<br>JACK IN",
    "WE ARE ON EARTH<br>THERE IS NO<br>CURE FOR THAT",
    "THE BASTARD FORM OF MASS CULTURE IS HUMILIATED REPETITION... ALWAYS NEW BOOKS, NEW PROGRAMS, NEW FILMS, NEW ITEMS, BUT ALWAYS THE SAME MEANING",
    "WE WILL FEEL OUR ACTIONS ON THE WEB NOT AS SMALL PEBBLES TOSSED INTO A STILL VOID, BUT AS TINY ALTERATIONS OR ADDITIONS TO A GIANT, SPINNING, INDEPENDENT STORM",
    "NUMB THYSELF",
    "THE GREATEST IRONY INSTEAD OF THE HIGHEST AUTHORITY",
    "JOKES INSTEAD OF HEAVINESS",
    "QUESTIONS INSTEAD OF CERTAINTY",
    "NONDESCRIPT STUFF TRANSFORMED IN THE MIND INSTEAD OF SOULS SAVED BY NATURE",
    "TAPPING INTO CIVILIZATION THROUGH YOUR OWN FEELINGS",
    "BLANKNESS<br>THE FAVORITE<br>FEELING OF NOW",
    "PERHAPS IT'S SHARE NATURALISM THAT IS SO VIOLENT",
    "WHY?",
    "THERE IS<br>SOME OF ME<br>IN YOU",
    "BEAUTY IS ALWAYS THE RESULT OF AN ACCIDENT",
    "IT'S ADVERTISING MARRYING MINIMALISM AND HAVING<br>ROSEMARYS BABY",
    "ART<br>WE ONLY WISH<br>TO SERVE<br>YOU",
    "MANS DESIRES MUST<br>OVERSHADOW<br>HIS NEEDS",
    "IN THE FUTURE EVERYONE WILL HAVE A PHYSICS ENGINE",
    "I REFUSE TO PARTICIPATE IN A RECESSION",
    "YOUR GENES WILL LIVE FOREVER",
    "ALL WATCHED OVER BY MACHINES OF LOVING GRACE",
    "ENERGY IS ETERNAL DELIGHT",
    "YOUR SOUL IS DIGITAL",
    "EVERYTHING IS A COPY OF A COPY OF A COPY",
    "EVERY REFORM MOVEMENT HAS A LUNATIC FRINGE"
];

var _selected_poster = null;

var font_cleaner = {
        
    checkOffsetWidth: function(family, size) {
        var node = document.createElement("p");        
        $(node).css("font-family", "'" + family + "', Times New Roman");    
        $(node).css("font-size", size);
        $(node).css("display", "inline");
        $(node).addClass("font-test")
            
        // This was from http://www.lalit.org/lab/javascript-css-font-detect
        $(node).html("mmmmmmmmml"); // m or w take up max width
        $("body").append(node);
        var width = node.offsetWidth;
        $("body p.font-test").remove();
        return width;
    },
        
    fallbackWidth: function() {
        if (!this._fallbackWidthCache) this._fallbackWidthCache = this.checkOffsetWidth("Times New Roman", "120px");
        return this._fallbackWidthCache;
    },
        
    checkFont: function(family) {
        // We use Times New Roman as a fallback
        if (family == "Times New Roman") return true;    
        // Ignore fonts like: 'Arno Pro Semibold 18pt'
        if (/\d+pt\s*$/.test(family)) return false;
        var familyWidth = this.checkOffsetWidth("'" + family + "', Times New Roman", "120px");
        return (familyWidth != this.fallbackWidth());
    },
        
    filterFonts: function(fonts) {
        var filtered = []; 
        for (var i = 0, length = fonts.length; i < length; i++) {
            if (this.checkFont(fonts[i].fontName)){
                var ignore_font = false;
                for(var j in IGNORE){
                    if(fonts[i].fontName == IGNORE[j]){
                        ignore_font = true;
                        break;
                    }
                }
                if(!ignore_font){
                    filtered.push(fonts[i]);
                }
            }
        }
        return filtered;
    }
};

function randomid(length){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
    
function randomcolor(){
    var c = new Color();
    c.randomize();
    return c.hexString();
}
    
function randomfont(fonts){
    return fonts[Math.floor((Math.random()*fonts.length))].fontName;
}
    
function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex){
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });
    
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function formatbackg(color){
    if(color.type == "color"){
        return "background-color:"+color.firstcolor+";";
    }
    return "background-image: -webkit-linear-gradient(top, "+color.firstcolor+" "+color.firststop+"%, "+color.secondcolor+" "+color.secondstop+"%);";
    
}
    
function randombackg(){
    
    var backg = {
        type: "color",
        firstcolor: "",
        secondcolor: "",
        firststop: 0,
        secondstop: 0
    };
        
    switch(MODE){
            
        case "free":
        case "black-text":
        case "white-text":
            var isgradient = Math.random() * 1.0;
            if(isgradient > .5){
                backg.firstcolor = randomcolor();
                return backg;
            }
                
            var firstcolor = randomcolor();
            var secondcolor = randomcolor();
                
            var firststop = Math.round((0.5 - (Math.random() * .5 ))* 100);
            var secondstop = Math.round((0.5 + (Math.random() * .5 ))* 100);
            
            var firststop = Math.round(0);
            var secondstop = Math.round(100);
            
            backg.type = "gradient";
            backg.firstcolor = firstcolor;
            backg.secondcolor = secondcolor;
            backg.firststop = firststop;
            backg.secondstop = secondstop;
                
            return backg;
                
            
        case "black-and-white":
                
            var isblack = Math.random() * 1.0;
                
            if(isblack > .5){
                backg.firstcolor = BLACK;
                return backg;
            }
            
            backg.firstcolor = WHITE;
            return backg;
                
        case "grayscale-backg":
        case "grayscale-text-backg":
            
            var scale = Math.round(Math.random()*255);
            backg.firstcolor = new Color({r:scale,g:scale,b:scale}).hexString();
            return backg;
        
        case "plain-color-backg":
        case "plain-color-backg-black":
        case "plain-color-backg-white":
        case "plain-color-backg-complement":
        case "plain-color-backg-lighter":
        case "plain-color-backg-darker":
            backg.firstcolor = randomcolor();
            return backg;
    }
}
    
function onFontDetectReady(){
        
    var swfElement = document.getElementById(FLASH_ID);
    FONTS = font_cleaner.filterFonts(swfElement.fonts());
        
    var options = "";
        
    for(var i in FONTS){
        options += "<option value='"+FONTS[i].fontName+"'>"+FONTS[i].fontName+"</option>";
    }
        
    $("#poster-controls #fonts").html(options);
    
    createposters();
        
}

function createposter(quote){
    
    var thefont = randomfont(FONTS);
    var thecolor = randomcolor();
    var thebackg = randombackg();
            
    switch(MODE){
                
        case "plain-color-backg-black":
        case "black-text":
            thecolor = BLACK;
            break;
                    
        case "white-text":
        case "plain-color-backg-white":
            thecolor = WHITE;
            break;
        case "black-and-white":
            if(thebackg.firstcolor == BLACK){
                thecolor = WHITE;
                break;
            }
            thecolor = BLACK;       
            break;
                
        case "grayscale-text-backg":
            var scale = Math.round(Math.random()*255);
            thecolor = rgbToHex(scale, scale, scale);
        break;
        
        case "plain-color-backg-complement":
            var input = new Color(thebackg.firstcolor);
            var comp = input.complement();
            thecolor = comp.hexString();
        break;
        
        case "plain-color-backg-lighter":
            var transcolor = new Color(thebackg.firstcolor); 
            thecolor = transcolor.lighten(.1 + Math.random()* .9).hexString();
        break;
        
        case "plain-color-backg-darker":
            var transcolor = new Color(thebackg.firstcolor); 
            thecolor = transcolor.darken(.1 + Math.random()* .9).hexString();
        break;
        
    }
    
    var size = DEFAULT_SIZE + (6 - Math.round(Math.random() * 6));
    
    var options = {
        width: POSTER_WIDTH,
        height: POSTER_HEIGHT,
        firstcolor: thebackg.firstcolor,
        secondcolor: thebackg.secondcolor,
        firststop: thebackg.firststop,
        secondstop: thebackg.secondstop,
        color: thecolor,
        font: thefont,
        size: size,
        quote: quote,
        formatedbackg: formatbackg(thebackg)
    };
    
    return $(postertemplate(options));
}

function loadtemplate(url, options, callback){
    var data = "";
    $.ajax({
        async: false,
        url: url,
        success: function(response) {
            data = response;
            var compiled = _.template(data);
            var tmp = compiled(options);
            if(callback){
                callback(tmp);
            }
        }
    });
    
}
 
function postertemplate(options){
    var tmp = "<div class='poster' data-width='"+options.width+"' data-height='"+options.height+"' data-backg-first-color='"+options.firstcolor+"' data-backg-first-stop='"+options.firststop+"' data-backg-second-color='"+options.secondcolor+"' data-backg-second-stop='"+options.secondstop+"' data-color='"+options.color+"' data-font='"+options.font+"' data-font-size='"+options.size+"' data-line-height='1.6' data-letter-spacing='0' style='font-size:"+options.size+"px;font-family:\""+options.font+"\";color:"+options.color+";width:"+options.width+"px;height:"+options.height+"px;"+options.formatedbackg+"'><div class='cell'><div class='content'>"+options.quote+"</div></div><div class='font-name'>"+options.font+"</div><div class='settings'><a title='export' class='export' href='#'><span>Export</span></a></a><a title='Refresh' class='refresh' href='#'><span>Refresh</span></a><a title='Edit' class='edit' href='#'><span>Edit</span></a></div></div>";

    return tmp;
 
}

function exportoptions(el){
    
    var isgradient = false;
    var secondcolor = el.data("backg-second-color");
    if(secondcolor != ""){
        isgradient = true;
        secondcolor = hexToRgb(el.data("backg-second-color"));
    }else{
        secondcolor = hexToRgb(el.data("backg-first-color"));
    }

    var options = {
        padding: PADDING,
        width: el.data("width"),
        height: el.data("height"),
        firstcolor: hexToRgb(el.data("backg-first-color")),
        secondcolor: secondcolor,
        size: el.data("font-size"),
        font: el.data("font"),
        quote: $(".cell .content", el).html(),
        color: hexToRgb(el.data("color")),
        backgcolor: hexToRgb(el.data("backg-first-color")), 
        isgradient: isgradient
    };
    return options;
}

function exportscript(options, callback){

    loadtemplate('templates/export.tmpl', options, callback);
}

function fixheight(el, maxheight){
    
    if(el.outerHeight() > maxheight){
        var current_size = el.data("font-size");
        el.data("font-size", current_size - 1);
        el.css("font-size", current_size - 1);

        fixheight(el, maxheight);
    }
}

function createposters(){
    
    $("#wrapper").html("");
    
    for(var i in QUOTES){
        
        var obj = createposter(QUOTES[i]);

        $("#wrapper").append(obj);
    }

    $(".poster").each(function(index){
        
        if($(this).outerHeight() > POSTER_HEIGHT){
            fixheight($(this), POSTER_HEIGHT);            
        }
    });
}
    
$(document).ready(function(){
        
    // Check Flash version
    if( !swfobject.hasFlashPlayerVersion("9.0.0") ){
        console.log("need flash 9 or later");
    }else{
        var flashvars = {onReady: "onFontDetectReady", swfObjectId: FLASH_ID};
        var params = {allowScriptAccess: "always", menu: "false"};
        var attributes = {id: FLASH_ID, name: FLASH_ID};
        swfobject.embedSWF(FLASH_PATH, FLASH_ID, "1", "1", "9.0.0", null, flashvars, params, attributes); 
            
        $(".poster .edit").live("click", function(e){
            e.preventDefault();
                
            _selected_poster = $(this).parent().parent();
                
            var pos = _selected_poster.position();
                
            $("#poster-controls").css("left", pos.left+340);
            $("#poster-controls").css("top", pos.top+58);
                
            // the quote
            $("#poster-controls #quote").val($(".content", _selected_poster).html().replace(/<br>/g, "\n"));
                
            // text color
            $("#poster-controls #text-color").val( _selected_poster.data("color") );
                
            // font
            $("#poster-controls #fonts").val( _selected_poster.data("font"));
                
            // font size
            $("#poster-controls #font-size").val( _selected_poster.data("font-size"));
                
            // line height
            $("#poster-controls #line-height").val( _selected_poster.data("line-height"));
                
            // letter spacing
            $("#poster-controls #letter-spacing").val( _selected_poster.data("letter-spacing"));

            // backgcolor
            var first_color = _selected_poster.data("backg-first-color");
            var second_color = _selected_poster.data("backg-second-color");

            $("#poster-controls #backg-color-start").val(first_color);
            
            if(second_color == ""){

                $("#poster-controls #backg-color-end").val(first_color);
                $("#poster-controls #backg-color-end").prop('disabled', true);

            }else{
                $("#poster-controls #backg-color-end").val(second_color);
                $("#poster-controls #backg-color-end").prop('disabled', false);
            }

            $("#poster-controls").show();
                
        });
        
        $(".poster .refresh").live("click", function(e){
            e.preventDefault();
                
            var target = $(this).parent().parent();
            var quote = $(".content", target).html();
            
            var newposter = createposter(quote);
            
            target.replaceWith(newposter);
            
        });
        
        $(".poster .export").live("click", function(e){
            e.preventDefault();

            var target = $(this).parent().parent();

            var myoptions = exportoptions(target);

            exportscript(myoptions, function(result){
                var supported = true;
                try { 
                    var isFileSaverSupported = !!new Blob(); 
                } catch(e){
                    supported = false;
                }
                if(!supported){
                    $("#poster-output textarea").html(result);
                    $("#poster-output").show();
                    $("#poster-output textarea").focus();
                    $("#poster-output textarea").select();
                }else{
                    var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
                    var filename = randomid(8);
                    saveAs(blob, filename+".jsx");
                }
            });
            

        });
        
        $(".poster .like").live("click", function(e){
            e.preventDefault();
            
            var target = $(this).parent().parent();
            
        });
        
        
            
        $("#close-btn").on("click", function(e){
            e.preventDefault();
            $("#poster-controls").hide();
                
            _selected_poster = null;
        });
            
        $("#poster-controls #quote").on("change", function(e){
                
            if(_selected_poster){
                $(".content", _selected_poster).html($(this).val().replace(/\n/g, "<br>"));
            }
                
        });
            
        $("#poster-controls #font-size").on("change", function(e){
                
            if(_selected_poster){
                _selected_poster.css("font-size", $(this).val()+"px");
                _selected_poster.data("font-size", $(this).val());
            }
                
        });
            
        $("#poster-controls #line-height").on("change", function(e){
                
            if(_selected_poster){
                _selected_poster.css("line-height", $(this).val()+"em");
                _selected_poster.data("line-height", $(this).val());
            }
                
        });
            
        $("#poster-controls #letter-spacing").on("change", function(e){
                
            if(_selected_poster){
                _selected_poster.css("letter-spacing", $(this).val()+"px");
                _selected_poster.data("letter-spacing", $(this).val());
            }
                
        });
            
        $("#poster-controls #text-color").on("change", function(e){
                
            if(_selected_poster){
                _selected_poster.css("color", $(this).val());
                _selected_poster.data("color", $(this).val());
            }
                
        });
            
        $("#poster-controls #fonts").on("change", function(e){
            if(_selected_poster){
                _selected_poster.css("font-family", $(this).val());
                _selected_poster.data("font", $(this).val());
            }
        });

        $("#poster-controls #backg-color-start, #poster-controls #backg-color-end").on("change", function(e){
            
            if(_selected_poster){
                var is_first = false;

                if($(this).attr("id") == "backg-color-start"){
                    is_first = true;
                }

                var first_color = _selected_poster.data("backg-first-color");
                var second_color = _selected_poster.data("backg-second-color");
                var first_stop = _selected_poster.data("backg-first-stop");
                var second_stop = _selected_poster.data("backg-second-stop");
                
                if(second_color == ""){
                    
                    if(is_first){
                        _selected_poster.css("background-color", $(this).val());
                        
                        $("#poster-controls #backg-color-end").val($(this).val());
                    
                        _selected_poster.data("backg-first-color", $(this).val());
                    }

                }else{
                    
                    var n_first_color = "";
                    var n_second_color = "";
                    
                    if(is_first){

                        n_first_color = $(this).val();
                        n_second_color = second_color;

                    }else{
                        n_first_color = first_color;
                        n_second_color = $(this).val();
                    }

                    _selected_poster.data("backg-first-color", n_first_color);
                    _selected_poster.data("backg-second-color", n_second_color);
                    
                    _selected_poster.css("background-image", "-webkit-linear-gradient(top, "+n_first_color+" "+first_stop+"%, "+n_second_color+" "+second_stop+"%)");
                    //-webkit-linear-gradient(top, "+color.firstcolor+" "+color.firststop+"%, "+color.secondcolor+" "+color.secondstop+"%);";

                }
            }

        });
            
        $("#main-controls #mode").on("change", function(e){
                
            MODE = $(this).val();
                
            createposters();
                
            _selected_poster = null;
        });
        
        $("#refresh-btn").click(function(e){
            e.preventDefault();
            createposters();
        });

        $("#poster-output .close-btn").on("click", function(e){
            e.preventDefault();
            $("#poster-output").hide();
        });
        
      
    }
        
});
